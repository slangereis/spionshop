﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Spionshop.Models;

namespace Spionshop.Controllers
{
    public class BestellingDetailsController : ApiController
    {
        private SpionshopEntities db = new SpionshopEntities();

        // GET: api/BestellingDetails
        public IQueryable<BestellingDetail> GetBestellingDetail()
        {
            return db.BestellingDetail;
        }

        // GET: api/BestellingDetails/5
        [ResponseType(typeof(BestellingDetail))]
        public async Task<IHttpActionResult> GetBestellingDetail(int id)
        {
            BestellingDetail bestellingDetail = await db.BestellingDetail.FindAsync(id);
            if (bestellingDetail == null)
            {
                return NotFound();
            }

            return Ok(bestellingDetail);
        }

        // PUT: api/BestellingDetails/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBestellingDetail(int id, BestellingDetail bestellingDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bestellingDetail.BD_id)
            {
                return BadRequest();
            }

            db.Entry(bestellingDetail).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BestellingDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/BestellingDetails
        [ResponseType(typeof(BestellingDetail))]
        public async Task<IHttpActionResult> PostBestellingDetail(BestellingDetail bestellingDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.BestellingDetail.Add(bestellingDetail);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = bestellingDetail.BD_id }, bestellingDetail);
        }

        // DELETE: api/BestellingDetails/5
        [ResponseType(typeof(BestellingDetail))]
        public async Task<IHttpActionResult> DeleteBestellingDetail(int id)
        {
            BestellingDetail bestellingDetail = await db.BestellingDetail.FindAsync(id);
            if (bestellingDetail == null)
            {
                return NotFound();
            }

            db.BestellingDetail.Remove(bestellingDetail);
            await db.SaveChangesAsync();

            return Ok(bestellingDetail);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BestellingDetailExists(int id)
        {
            return db.BestellingDetail.Count(e => e.BD_id == id) > 0;
        }
    }
}