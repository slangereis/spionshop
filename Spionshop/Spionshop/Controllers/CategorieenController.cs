﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Spionshop.Models;

namespace Spionshop.Controllers
{
    public class CategorieenController : ApiController
    {
        private SpionshopEntities db = new SpionshopEntities();

        // GET: api/Categorieen
        public IQueryable<Categorie> GetCategorie()
        {
            return db.Categorie;
        }

        // GET: api/Categorieen/5
        [ResponseType(typeof(Categorie))]
        public async Task<IHttpActionResult> GetCategorie(short id)
        {
            Categorie categorie = await db.Categorie.FindAsync(id);
            if (categorie == null)
            {
                return NotFound();
            }

            return Ok(categorie);
        }

        // PUT: api/Categorieen/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCategorie(short id, Categorie categorie)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != categorie.Cat_id)
            {
                return BadRequest();
            }

            db.Entry(categorie).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategorieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Categorieen
        [ResponseType(typeof(Categorie))]
        public async Task<IHttpActionResult> PostCategorie(Categorie categorie)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Categorie.Add(categorie);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = categorie.Cat_id }, categorie);
        }

        // DELETE: api/Categorieen/5
        [ResponseType(typeof(Categorie))]
        public async Task<IHttpActionResult> DeleteCategorie(short id)
        {
            Categorie categorie = await db.Categorie.FindAsync(id);
            if (categorie == null)
            {
                return NotFound();
            }

            db.Categorie.Remove(categorie);
            await db.SaveChangesAsync();

            return Ok(categorie);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategorieExists(short id)
        {
            return db.Categorie.Count(e => e.Cat_id == id) > 0;
        }
    }
}