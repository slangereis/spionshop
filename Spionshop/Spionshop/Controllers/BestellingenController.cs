﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Spionshop.Models;

namespace Spionshop.Controllers
{
    public class BestellingenController : ApiController
    {
        private SpionshopEntities db = new SpionshopEntities();

        // GET: api/Bestellingen
        public IQueryable<Bestelling> GetBestelling()
        {
            return db.Bestelling;
        }

        // GET: api/Bestellingen/5
        [ResponseType(typeof(Bestelling))]
        public async Task<IHttpActionResult> GetBestelling(int id)
        {
            Bestelling bestelling = await db.Bestelling.FindAsync(id);
            if (bestelling == null)
            {
                return NotFound();
            }

            return Ok(bestelling);
        }

        // PUT: api/Bestellingen/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBestelling(int id, Bestelling bestelling)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bestelling.B_id)
            {
                return BadRequest();
            }

            db.Entry(bestelling).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BestellingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Bestellingen
        [ResponseType(typeof(Bestelling))]
        public async Task<IHttpActionResult> PostBestelling(Bestelling bestelling)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Bestelling.Add(bestelling);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = bestelling.B_id }, bestelling);
        }

        // DELETE: api/Bestellingen/5
        [ResponseType(typeof(Bestelling))]
        public async Task<IHttpActionResult> DeleteBestelling(int id)
        {
            Bestelling bestelling = await db.Bestelling.FindAsync(id);
            if (bestelling == null)
            {
                return NotFound();
            }

            db.Bestelling.Remove(bestelling);
            await db.SaveChangesAsync();

            return Ok(bestelling);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BestellingExists(int id)
        {
            return db.Bestelling.Count(e => e.B_id == id) > 0;
        }
    }
}