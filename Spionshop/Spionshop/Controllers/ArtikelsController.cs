﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Spionshop.Models;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace Spionshop.Controllers
{
    public class ArtikelsController : ApiController
    {
        private SpionshopEntities db = new SpionshopEntities();

        // GET: api/Artikels
        public IQueryable<ArtikelDTO> GetArtikel()
        {
            Mapper.Initialize(cfg => cfg.CreateMap<Artikel, ArtikelDTO>());
            var artikels = db.Artikel.ProjectTo<ArtikelDTO>();

            return artikels;
        }

        // GET: api/Artikels/5
        [ResponseType(typeof(Artikel))]
        public async Task<IHttpActionResult> GetArtikel(short id)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<Artikel, ArtikelDetailDTO>());
            var artikel = await db.Artikel
                .ProjectTo<ArtikelDetailDTO>().SingleOrDefaultAsync(a => a.Artikel_id == id);
            if (artikel == null)
            {
                return NotFound();
            }

            return Ok(artikel);
        }

        // PUT: api/Artikels/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutArtikel(short id, Artikel artikel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != artikel.Artikel_id)
            {
                return BadRequest();
            }

            db.Entry(artikel).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArtikelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Artikels
        [ResponseType(typeof(ArtikelDTO))]
        public async Task<IHttpActionResult> PostArtikel(ArtikelDetailDTO artikelDetailDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var artikel = new Artikel
            {
                Artikel_id = artikelDetailDTO.Artikel_id,
                ArtikelNaam = artikelDetailDTO.ArtikelNaam,
                Cat_id = artikelDetailDTO.Cat_id,
                Omschrijving = artikelDetailDTO.Omschrijving,
                Instock = artikelDetailDTO.Instock,
                Verkoopprijs = artikelDetailDTO.Verkoopprijs
            };

            db.Artikel.Add(artikel);
            await db.SaveChangesAsync();

            return Ok(artikelDetailDTO);
        }

        // DELETE: api/Artikels/5
        [ResponseType(typeof(Artikel))]
        public async Task<IHttpActionResult> DeleteArtikel(short id)
        {
            Artikel artikel = await db.Artikel.FindAsync(id);
            if (artikel == null)
            {
                return NotFound();
            }

            db.Artikel.Remove(artikel);
            await db.SaveChangesAsync();

            return Ok(artikel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ArtikelExists(short id)
        {
            return db.Artikel.Count(e => e.Artikel_id == id) > 0;
        }
    }
}