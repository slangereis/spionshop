﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Spionshop.Models;

namespace Spionshop.Controllers
{
    public class KlantenController : ApiController
    {
        private SpionshopEntities db = new SpionshopEntities();

        // GET: api/Klanten
        public IQueryable<Klant> GetKlant()
        {
            return db.Klant;
        }

        // GET: api/Klanten/5
        [ResponseType(typeof(Klant))]
        public async Task<IHttpActionResult> GetKlant(short id)
        {
            Klant klant = await db.Klant.FindAsync(id);
            if (klant == null)
            {
                return NotFound();
            }

            return Ok(klant);
        }

        // PUT: api/Klanten/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutKlant(short id, Klant klant)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != klant.Klant_id)
            {
                return BadRequest();
            }

            db.Entry(klant).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KlantExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Klanten
        [ResponseType(typeof(Klant))]
        public async Task<IHttpActionResult> PostKlant(Klant klant)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Klant.Add(klant);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = klant.Klant_id }, klant);
        }

        // DELETE: api/Klanten/5
        [ResponseType(typeof(Klant))]
        public async Task<IHttpActionResult> DeleteKlant(short id)
        {
            Klant klant = await db.Klant.FindAsync(id);
            if (klant == null)
            {
                return NotFound();
            }

            db.Klant.Remove(klant);
            await db.SaveChangesAsync();

            return Ok(klant);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool KlantExists(short id)
        {
            return db.Klant.Count(e => e.Klant_id == id) > 0;
        }
    }
}