﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Spionshop.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Diagnostics;

    public partial class SpionshopEntities : DbContext
    {
        public SpionshopEntities()
            : base("name=SpionshopEntities")
        {
            Database.Log = s => Debug.WriteLine(s);
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Artikel> Artikel { get; set; }
        public virtual DbSet<Bestelling> Bestelling { get; set; }
        public virtual DbSet<BestellingDetail> BestellingDetail { get; set; }
        public virtual DbSet<Categorie> Categorie { get; set; }
        public virtual DbSet<Klant> Klant { get; set; }
    }
}
