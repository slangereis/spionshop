﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spionshop.Models
{
    public class ArtikelDTO
    {
        public short Artikel_id { get; set; }
        public string ArtikelNaam { get; set; }
    }

    public class ArtikelDetailDTO
    {
        public short Artikel_id { get; set; }
        public short Cat_id { get; set; }
        public string CategorieNaam { get; set; }
        public string ArtikelNaam { get; set; }
        public string Omschrijving { get; set; }
        public decimal? Verkoopprijs { get; set; }
        public short? Instock { get; set; }
    }

    public class BestellingDTO
    {
        public int B_id { get; set; }
        public short Klant_id { get; set; }
        public DateTime Datum { get; set; }
    }

    public class BestellingDetailDTO
    {
        public int BD_id { get; set; }
        public int B_id { get; set; }
        public short Artikel_id { get; set; }
        public short Aantal { get; set; }
    }

    public class CategorieDTO
    {
        public short Cat_id { get; set; }
        public string CategorieNaam { get; set; }
    }

    public class KlantDTO
    {
        public short Klant_id { get; set; }
        public string Naam { get; set; }
        public string Voornaam { get; set; }
        public string Woonplaats { get; set; }
        public DateTime? Geboortedatum { get; set; }
        public string Gebruikersnaam { get; set; }
        public string Pwd { get; set; }
    }
}